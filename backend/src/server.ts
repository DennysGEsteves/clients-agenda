import fastify from "fastify";
import cors from "@fastify/cors";
import { History, PrismaClient } from "@prisma/client";
import format from "date-fns/format";
import addDays from "date-fns/addDays";

const prisma = new PrismaClient();

type Client = {
  id: number;
  name: string;
  email: string;
  tel: string;
  address: string;
};

type HistoryPresenter = History & {
  client: Client | null
}

const app = fastify();

app.register(cors, {
  origin: "https://agenda-clientes.onrender.com",
  // origin: "http://localhost:3000",
});

app.get<{ Querystring: { page: number; search?: string } }>(
  "/users",
  async (req) => {
    const { page, search } = req.query;
    const where = {
      OR: [
        {
          name: {
            contains: search,
          },
        },
        {
          email: {
            contains: search,
          },
        },
      ],
    };

    const total = await prisma.client.count({ where });
    const clients = await prisma.client.findMany({
      skip: (page - 1) * 100,
      take: 100,
      where,
      orderBy: {
        name: "asc",
      },
    });
    return {
      total,
      data: clients,
    };
  }
);

app.post<{ Body: Client }>("/users", async (req, reply) => {
  await prisma.client.create({
    data: req.body,
  });

  reply.status(201).send();
});

app.put<{ Params: { id: number }; Body: Client }>(
  "/users/:id",
  async (req, reply) => {
    await prisma.client.update({
      where: {
        id: Number(req.params.id),
      },
      data: req.body,
    });

    reply.status(200).send();
  }
);

app.get<{ Params: { clientID: string } }>("/history/:clientID", async (req) => {
  const { clientID } = req.params;

  const history = await prisma.history.findMany({
    where: {
      clientID,
    },
    orderBy: {
      datefmt: "asc",
    },
  });

  return history;
});

app.get<{ Params: { startDate: string } }>(
  "/events/:startDate",
  async (req) => {
    const { startDate } = req.params;

    let date = new Date(startDate + " 00:00:00");
    const dates: { year: number; day: string }[] = [
      {
        year: Number(format(date, "yyyy")),
        day: format(date, "MM/dd"),
      },
    ];

    for (let x = 1; x < 7; x++) {
      date = addDays(date, 1);
      dates.push({
        year: Number(format(date, "yyyy")),
        day: format(date, "MM/dd"),
      });
    }

    const history = await prisma.history.findMany({
      where: {
        OR: dates,
      },
      orderBy: {
        datefmt: "asc",
      },
    });

    const presenter = [...history] as HistoryPresenter[]

    for (let i = 0; i < presenter.length; i++) {
      if (presenter[i].clientID) {
        presenter[i].client = await prisma.client.findFirst({
          where: { id: Number(history[i].clientID) },
        });
      } else {
        presenter[i].client = null
      }
    }

    return history;
  }
);

app
  .listen({
    host: "0.0.0.0",
    port: process.env.PORT ? Number(process.env.PORT) : 3001,
  })
  .then(() => {
    console.log(`Server is running on port ${process.env.PORT || 3001}`);
  });
