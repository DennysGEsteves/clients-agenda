"use client";
import { AgendaView } from "@/views/Agenda/Agenda.view";
import { ChakraProvider } from "@chakra-ui/react";

export default function PageView() {
  return (
    <ChakraProvider>
      <AgendaView />
    </ChakraProvider>
  );
}
