import ClientsView from "@/views/Clients/Clients.view";
import { useLogic } from "./Agenda.logic";
import { Calendar, Views } from "react-big-calendar";

import "react-big-calendar/lib/css/react-big-calendar.css";
import { localizer, messages } from "./Agenda.props";
import {
  Box,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";
import { parseISO } from "date-fns";

export const AgendaView = () => {
  const { data, methods } = useLogic();

  return (
    <Box p={5}>
      <Box textAlign={"right"} mb={5}>
        <Button variant={"solid"} colorScheme="blue" onClick={methods.onOpen}>
          Clientes
        </Button>
      </Box>
      <Box
        sx={{
          ".rbc-toolbar-label": {
            fontWeight: "bold",
            textTransform: "uppercase",
            fontSize: "20px",
          },
          ".rbc-event-content": {
            fontSize: "12px",
          },
          ".rbc-event-label": {
            display: 'none'
        }
        }}
      >
        <Calendar
          culture="pt-BR"
          localizer={localizer}
          events={data.events as object[]}
          style={{ height: "calc(100vh - 100px)" }}
          defaultView={Views.WEEK}
          views={[Views.WEEK, Views.DAY]}
          messages={messages}
          step={1}
          timeslots={15}
          min={parseISO('2023-04-25 06:00:00')}
          max={parseISO('2023-04-25 21:15:00')}
          onRangeChange={(date) => {
            methods.onRangeChange(date as Date[]);
          }}
        />
      </Box>
      <Modal
        isOpen={data.isOpen}
        onClose={methods.onClose}
        scrollBehavior="inside"
        size={"full"}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader></ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <ClientsView isOpen={data.isOpen} onClose={methods.onClose} />
          </ModalBody>
          <ModalFooter>
            <Button
              colorScheme="blue"
              variant={"ghost"}
              mr={3}
              onClick={() => {
                methods.onClose();
              }}
            >
              Fechar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
};
