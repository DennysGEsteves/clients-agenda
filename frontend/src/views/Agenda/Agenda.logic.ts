/* eslint-disable react-hooks/exhaustive-deps */
import { useDisclosure } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import {
  CalendarEvent,
  HistoryEvent,
  formatDate,
  startDayOfWeek,
} from "./Agenda.props";
import axios from "axios";
import { addMinutes } from "date-fns";

export const useLogic = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [events, setEvents] = useState<CalendarEvent[]>([]);

  const getEvents = async (day: string) => {
    const response = await axios.get(
      `https://clients-agenda.onrender.com/events/${day}`
      // `http://localhost:3001/events/${day}`
    );
    const historyEvents: CalendarEvent[] = response.data.map((event: HistoryEvent) => {
      return event.services
        ? {
            id: event.id,
            start: new Date(event.datefmt),
            end: addMinutes(new Date(event.datefmt), 15),
            title: formatTitle(
              `${event.client?.name ?? ""}${event.client?.name ? ' - ' : ''}${event.services}`
            ),
          }
        : undefined;
    })

    const unique = historyEvents.filter(
      (obj, index) =>
      historyEvents.findIndex(
          (item) => item?.title === obj?.title && item?.start === item?.start
        ) === index
    )
    

    setEvents(unique);
  };

  function formatTitle(str: string, times: number = 0): any {
    const title = str
      .replace("Ã§", "ç")
      .replace("Ã£", "ã")
      .replace("Ã¡", "á")
      .replace("Ãº", "ú")
      .replace("Ã", "í")
      .replace("í§", "ç")
      .replace("í‰", "É")
      .replace("íƒ", "Ã");

    times++;

    return times >= 3 ? title : formatTitle(title, times);
  };

  const onRangeChange = (date: Date[]) => {
    date && date.length ? getEvents(formatDate(date[0])) : [];
  };

  useEffect(() => {
    getEvents(startDayOfWeek);
  }, []);

  return {
    data: {
      isOpen,
      events,
    },
    methods: {
      onOpen,
      onClose,
      onRangeChange,
    },
  };
};
