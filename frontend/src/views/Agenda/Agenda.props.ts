import { dateFnsLocalizer, Messages } from "react-big-calendar";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay";
import ptBR from "date-fns/locale/pt-BR";

export const formatDate = (date: Date) => {
  console.log(date)
  return format(date, "yyyy-MM-dd");
};

export const startDayOfWeek = formatDate(startOfWeek(new Date()));

const locales = {
  "pt-BR": ptBR,
};

export const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

export const messages: Messages = {
  week: "Semana",
  day: "Dia",
  month: "Mês",
  previous: "Anterior",
  next: "Proximo",
  today: "Hoje",
};

type Client = {
  id: number;
  name: string;
  email: string;
  tel: string;
  address: string;
};

export type CalendarEvent = {
  id: number;
  title: string;
  start: Date;
  end: Date;
}  | undefined;

export type HistoryEvent = {
  id: number;
  services: string;
  datefmt: Date;
  client: Client | null
};
