export type Client = {
  id?: number;
  name: string;
  email: string;
  tel: string;
  address: string;
};

export type ClientsViewProps = {
  isOpen: boolean;
  onClose: () => void;
};
