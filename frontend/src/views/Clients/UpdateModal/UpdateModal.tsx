"use client";

import axios from "axios";

import {
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  FormErrorMessage,
  useToast,
  Button,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { Client } from "../Clients.props";

export type ClientErrors = {
  name: boolean;
  email: boolean;
};

const initialFormValues = {
  id: undefined,
  name: "",
  address: "",
  email: "",
  tel: "",
};

const initialFormErrors = {
  name: false,
  email: false,
};

export default function UpdateModal({
  isOpen,
  onClose,
  modalData,
  getClients,
}: any) {
  const [formValues, setFormValues] = useState<Client>(initialFormValues);
  const [formErrors, setFormErrors] = useState<ClientErrors>(initialFormErrors);

  const toast = useToast();

  const onSubmit = async () => {
    const isUpdate = !!formValues.id;

    if (validForm()) {
      axios[isUpdate ? "put" : "post"](
        `https://clients-agenda.onrender.com/users${
          isUpdate ? "/" + formValues.id : ""
        }`,
        // `http://localhost:3001/users${isUpdate ? "/" + formValues.id : ""}`,
        formValues
      )
        .then(() => {
          toast({
            title: `Cliente ${isUpdate ? "atualizado" : "criado"} com sucesso`,
            status: "success",
          });

          getClients(1);
          setFormValues({ ...initialFormValues });
          setFormErrors({ ...initialFormErrors });
          onClose();
        })
        .catch(() => {
          toast({
            title: `Erro ao tentar ${
              isUpdate ? "criar" : "atualizar"
            } o cliente`,
            status: "error",
          });
        });
    }
  };

  const validForm = () => {
    let isValid = true;
    const errors = {
      name: false,
      email: false,
    };

    if (!formValues.name.length) {
      isValid = false;
      errors.name = true;
    }
    if (!formValues.email.length) {
      isValid = false;
      errors.email = true;
    }

    setFormErrors({ ...errors });
    return isValid;
  };

  useEffect(() => {
    console.log(modalData);
    setFormValues(modalData || initialFormValues);
  }, [modalData]);

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader></ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <FormControl isInvalid={formErrors.name}>
            <FormLabel>Nome</FormLabel>
            <Input
              defaultValue={formValues.name}
              onChange={(e) =>
                setFormValues({ ...formValues, name: e.target.value })
              }
            />
            <FormErrorMessage>* obrigatório</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={formErrors.email}>
            <FormLabel>Email</FormLabel>
            <Input
              type="email"
              defaultValue={formValues.email}
              onChange={(e) =>
                setFormValues({ ...formValues, email: e.target.value })
              }
            />
            <FormErrorMessage>* obrigatório</FormErrorMessage>
          </FormControl>
          <FormControl>
            <FormLabel>Telefone</FormLabel>
            <Input
              defaultValue={formValues.tel}
              onChange={(e) =>
                setFormValues({ ...formValues, tel: e.target.value })
              }
            />
            <FormErrorMessage>* obrigatório</FormErrorMessage>
          </FormControl>
          <FormControl>
            <FormLabel>Endereço</FormLabel>
            <Input
              defaultValue={formValues.address}
              onChange={(e) =>
                setFormValues({ ...formValues, address: e.target.value })
              }
            />
            <FormErrorMessage>* obrigatório</FormErrorMessage>
          </FormControl>
        </ModalBody>
        <ModalFooter>
          <Button
            colorScheme="blue"
            variant={"ghost"}
            mr={3}
            onClick={() => {
              setFormValues({ ...initialFormValues });
              setFormErrors({ ...initialFormErrors });
              onClose();
            }}
          >
            Fechar
          </Button>
          <Button variant="solid" colorScheme="blue" onClick={onSubmit}>
            Salvar
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
