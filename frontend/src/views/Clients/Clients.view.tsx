"use client";

import { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Button,
  useColorModeValue,
  Input,
  useDisclosure,
} from "@chakra-ui/react";
import { AddIcon, Search2Icon } from "@chakra-ui/icons";
import { ChakraProvider } from "@chakra-ui/react";
import DataTable, { TableColumn } from "react-data-table-component";
import axios from "axios";
import UpdateModal from "./UpdateModal/UpdateModal";
import ClientHistory from "@/views/Clients/History/ClientHistory";
import { Client, ClientsViewProps } from "./Clients.props";

const timeout: any = null;

export const ClientsView = (props: ClientsViewProps) => {
  const [data, setData] = useState<Client[]>([]);
  const [totalRows, setTotalRows] = useState(0);
  const [modalData, setModalData] = useState<Client | null>(null);
  const [historyClientId, setHistoryClientId] = useState<number | null>(null);
  const [search, setSearch] = useState("");
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isHistoryOpen,
    onOpen: onHistoryOpen,
    onClose: onHistoryClose,
  } = useDisclosure();

  const columns: TableColumn<Client>[] = [
    {
      name: "Nome",
      selector: (row: Client) => row.name,
    },
    {
      name: "Email",
      selector: (row: Client) => row.email,
    },
    {
      name: "Telefone",
      selector: (row: Client) => row.tel,
    },
    {
      name: "Endereço",
      selector: (row: Client) => row.address,
    },
    {
      name: "Ação",
      button: true,
      width: "200px",
      cell: (row) => (
        <>
          <Button
            variant={"outline"}
            size={"xs"}
            colorScheme="blue"
            sx={{ marginRight: 2 }}
            onClick={() => {
              setModalData({ ...row });
              onOpen();
            }}
          >
            Editar
          </Button>
          <Button
            variant={"outline"}
            size={"xs"}
            colorScheme="cyan"
            // sx={{ marginRight: 2 }}
            onClick={() => {
              setHistoryClientId(row.id!);
              onHistoryOpen();
            }}
          >
            histórico
          </Button>
          {/* <Button variant={"outline"} size={"xs"} colorScheme="red">
            Remover
          </Button> */}
        </>
      ),
    },
  ];

  const getClients = async (page: number) => {
    const response = await axios.get(
      `https://clients-agenda.onrender.com/users?page=${page}&search=${search}`
      // `http://localhost:3001/users?page=${page}&search=${search}`
    );
    setData(response.data.data);
    setTotalRows(response.data.total);
  };

  const handlePageChange = (page: number) => {
    getClients(page);
  };

  const onSearch = () => {
    getClients(1);
  };

  useEffect(() => {
    getClients(1); // fetch page 1 of users
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ChakraProvider>
      <Box bg={useColorModeValue("gray.100", "gray.900")} px={4}>
        <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
          <Flex w={"50%"} align={"center"}>
            <Input
              sx={{
                bg: "white",
              }}
              placeholder="Procure pelo nome ou email do cliente"
              defaultValue={""}
              onChange={(e) => setSearch(e.target.value)}
            />
            <Button
              variant={"outline"}
              colorScheme={"teal"}
              ml={4}
              leftIcon={<Search2Icon />}
              onClick={() => {
                getClients(1);
              }}
            >
              procurar
            </Button>
          </Flex>
          <Button
            variant={"solid"}
            colorScheme={"teal"}
            mr={4}
            leftIcon={<AddIcon />}
            onClick={() => {
              setModalData(null);
              onOpen();
            }}
          >
            Adicionar
          </Button>
        </Flex>
      </Box>
      <Box p={4}>
        <DataTable
          title=""
          columns={columns}
          data={data}
          dense
          pagination
          paginationServer
          paginationTotalRows={totalRows}
          paginationRowsPerPageOptions={[100]}
          paginationPerPage={100}
          onChangePage={handlePageChange}
          fixedHeader
          fixedHeaderScrollHeight="calc(90vh - 50px)"
          customStyles={{
            head: {
              style: {
                fontWeight: "bold",
                fontSize: "16px",
              },
            },
            headCells: {
              style: {
                background: "black",
                color: "white",
              },
            },
          }}
        />
      </Box>
      <UpdateModal
        isOpen={isOpen}
        onClose={onClose}
        modalData={modalData}
        getClients={getClients}
      />
      <ClientHistory
        isOpen={isHistoryOpen}
        onClose={onHistoryClose}
        clientId={historyClientId}
      />
    </ChakraProvider>
  );
};

export default ClientsView;
