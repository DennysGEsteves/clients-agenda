"use client";

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from "@chakra-ui/react";
import axios from "axios";
import { useEffect, useState } from "react";

export default function ClientHistory({ isOpen, onClose, clientId }: any) {
  const [history, setHistory] = useState([]);

  const getHistory = async (clientId: number) => {
    const response = await axios.get(
      `https://clients-agenda.onrender.com/history/${clientId}`
      // `http://localhost:3001/history/${clientId}`
    );
    setHistory(response.data);
  };

  const getDate = (dateStr: string) => {
    const date = dateStr.replace(".000Z", "").replace("T", " ");
    const [fullDay, fullHour] = date.split(" ");
    const [year, month, day] = fullDay.split("-");
    const [hour, minutes] = fullHour.split(":");

    return `${day}/${month}/${year} ${hour}:${minutes}`;
  };

  useEffect(() => {
    if (clientId) {
      getHistory(clientId);
    }
  }, [clientId]);

  return (
    <Modal isOpen={isOpen} onClose={onClose} scrollBehavior="inside" size={"4xl"}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader></ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <TableContainer>
            <Table variant="simple">
              {/* <TableCaption>Imperial to metric conversion factors</TableCaption> */}
              <Thead>
                <Tr>
                  <Th>Data</Th>
                  <Th>Serviços</Th>
                </Tr>
              </Thead>
              <Tbody>
                {history.map((data: any) => (
                  <Tr key={data.id}>
                    <Td>{getDate(data.datefmt)}</Td>
                    <Td>{data.services}</Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </ModalBody>
        <ModalFooter>
          <Button
            colorScheme="blue"
            variant={"ghost"}
            mr={3}
            onClick={() => {
              onClose();
            }}
          >
            Fechar
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
